#include "../include/MainThread.h"

MainThread::MainThread(std::shared_ptr<std::unordered_map<std::string, json::JSON>> config) :
	_config(std::move(config)) {

	_currencyAPI = std::make_shared<CurrencyAPI>(_config->at("CurrencyAPI"));
	_httpThread = std::make_unique<HTTPClient>(_config->at("HttpClient"), _currencyAPI);
	_forexAnalyzer = std::make_unique<ForexAnalyzer>(_config->at("ForexAnalyzer"), _currencyAPI);
}

MainThread::~MainThread() {
	Stop();
}

void MainThread::Run() {

	_httpThread->Run();
	_forexAnalyzer->Run();
}

void MainThread::Stop() {

	_httpThread->Stop();
	_forexAnalyzer->Stop();
}
