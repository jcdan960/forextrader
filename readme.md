# ForexTrader
### Dependancies
**OpenSSL 1.1.1**

**Windows** : 
* Follow these instructions : https://www.xolphin.com/support/OpenSSL/OpenSSL_-_Installation_under_Windows

**Linux**: 
* OpenSSL is likely already installed in /usr/lib/x86-64-linux-gnu or something like that. Just try to run the CMake and see if it works.


** In case you can't find the binaries or you need to built it, the source can be found here: https://gitlab.com/jici/openssl
