#ifndef MAINTHREAD_H
#define MAINTHREAD_H
#include <include/httpclient.h>
#include <json.hpp>
#include <include/ForexAnalyzer.h>
#include <include/CurrencyAPI.h>

class MainThread  {

public:
	MainThread(std::shared_ptr<std::unordered_map <std::string, json::JSON>> config);
	~MainThread();
	void Run();

	void Stop();

private:
	std::unique_ptr<HTTPClient> _httpThread;
	std::unique_ptr<ForexAnalyzer> _forexAnalyzer;
	std::shared_ptr<CurrencyAPI> _currencyAPI;
	std::shared_ptr<std::unordered_map <std::string, json::JSON>>  _config;
};

#endif // !MAINTHREAD_H
